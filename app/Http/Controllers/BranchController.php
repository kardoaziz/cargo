<?php

namespace App\Http\Controllers;

use App\Branch;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('branches.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('branches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:191',
        ]);
        $branch = new Branch();
        $branch->name = $request->name;
        $branch->phone = $request->phone;
        $branch->id_user = $request->id_user;
        $branch->address = $request->address;
        $branch->description = $request->description;

        $branch->save();
        return redirect(route('branches.index'))->with('success', trans('main.Branch Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branch::find($id);
        return view('branches.edit',compact('branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:191',
        ]);
        $branch =  Branch::find($id);
        $branch->name = $request->name;
        $branch->phone = $request->phone;
        $branch->id_user = $request->id_user;
        $branch->address = $request->address;
        $branch->description = $request->description;
        $branch->save();
        return redirect(route('branches.index'))->with('success', trans('main.Branch Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch = Branch::find($id);
        $branch->destroy($id);
        return redirect(route('branches.index'))->with('success', trans('main.branch Deleted Successfully'));
    }
}
