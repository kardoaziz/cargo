<?php

namespace App\Http\Controllers;

use App\LocationPrice;
use App\Location;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LocationPriceController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $CostDate = LocationPrice::select('created_at')->distinct()->orderby('created_at','desc')->get();
        // $CostDate=LocationPrice::select('from_location','to_location')->distinct()->orderby('id','desc')->get();
        return view('location_cost.index',['costDate'=>$CostDate]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations=Location::all();

        return view('location_cost.create',['locations'=>$locations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            foreach($request->from as $key => $val){
                $data =array(
                    'from_location' => $request->from[$key],
                    'to_location' => $request->to[$key],
                    'price' => $request->price[$key],
                    // 'status' => true,
                    'created_at' => Carbon::now(),
                );
                LocationPrice::insert($data);
            }
            return redirect(route('locationCost.index'))->with('success', trans('main.price Deleted Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LocationCost  $locationCost
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

       $prices=LocationPrice::where('created_at',$id)->get();
       return view('location_cost.show',['prices'=>$prices,'data'=>$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LocationCost  $locationCost
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LocationCost  $locationCost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LocationCost $locationCost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LocationCost  $locationCost
     * @return \Illuminate\Http\Response
     */
    public function destroy(LocationCost $locationCost)
    {
        //
    }
}
