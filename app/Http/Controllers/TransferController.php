<?php

namespace App\Http\Controllers;

use App\Transfer;
use Illuminate\Http\Request;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('transfers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transfers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'from_user' => 'required|not_in:0',
            'to_user' => 'required|not_in:0',
            'date' => 'required|date',
            'amount' => 'required|numeric',
        ]);

        $transfer =new Transfer();
        $transfer->from_user=$request->from_user;
        $transfer->to_user=$request->to_user;
        $transfer->amount=$request->amount;
        $transfer->date=$request->date;
        $transfer->note=$request->note;
        $transfer->status="pending";

        $transfer->save();
        return redirect(route('transfer.index'))->with('success', trans('main.Transfer Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function show(Transfer $transfer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transfer=Transfer::find($id);
        return view('transfers.edit',compact('transfer'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transfer =Transfer::find($id);
        if ($request->status==null) {
            $request->validate([
                'from_user' => 'required|not_in:0',
                'to_user' => 'required|not_in:0',
                'date' => 'required|date',
                'amount' => 'required|numeric',
            ]);
            $transfer->from_user=$request->from_user;
            $transfer->to_user=$request->to_user;
            $transfer->amount=$request->amount;
            $transfer->date=$request->date;
            $transfer->note=$request->note;
            $transfer->status="pending";
            $transfer->save();
        }else{
            $transfer->status=$request->status;
            $transfer->save();
        }


        return redirect(route('transfer.index'))->with('success', trans('main.Transfer Updated Successfully'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transfer = Transfer::find($id);
        $transfer->destroy($id);
        return redirect(route('transfer.index'))->with('success', trans('main.Trasnfer Deleted Successfully'));
    }
}
