<?php

namespace App\Http\Controllers;

use App\ExpressItem;
use Illuminate\Http\Request;

class ExpressItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExpressItem  $expressItem
     * @return \Illuminate\Http\Response
     */
    public function show(ExpressItem $expressItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExpressItem  $expressItem
     * @return \Illuminate\Http\Response
     */
    public function edit(ExpressItem $expressItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExpressItem  $expressItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExpressItem $expressItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExpressItem  $expressItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExpressItem $expressItem)
    {
        //
    }
}
