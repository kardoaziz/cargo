<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Express;
use Auth;
class MyTravelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('mytravels.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
          $express = Express::find($id);
            return view('mytravels.show',compact('express'));
    }

    public function filter(Request $request)
    {
        // return $request;
        $expresses = Express::join('express_statuses','express_statuses.id_express','expresses.id')->select('expresses.id','expresses.from_location','expresses.to_location','expresses.from_branch','expresses.from_branch','expresses.created_at','expresses.total_delivery','expresses.status','expresses.package_price','expresses.id_store','expresses.tracking_no','expresses.dest_address','expresses.dest_phone')->where('expresses.id_store', Auth::user()->id_store)->where('expresses.status',$request->status);

        if(isset($request->from_date))
        {
            $expresses=$expresses->where('expresses.created_at','>=',$request->from_date);
        }
        if(isset($request->to_date))
        {
            $expresses=$expresses->where('expresses.created_at','<=',$request->to_date);

        }
        $expresses=$expresses->distinct()->orderby('id','desc')->get();
        // return $expresses;
        $status = $request->status;
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        return view('mytravels.index',compact('expresses','status','from_date','to_date'));
    }
    public function printselected(Request $request)
    {
        // return $request;
        $ids = explode(',', $request->id_expressesp);
        $expresses = Express::whereIn('id',$ids)->get();
        return view('mytravels.print',compact('expresses'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
