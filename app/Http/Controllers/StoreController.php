<?php

namespace App\Http\Controllers;

use App\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('stores.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('stores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:191',
            'address' => 'required|max:191',
            'id_branch' => 'required|not_in:0',
            'id_location' => 'required|not_in:0',
        ]);
        $store = new Store();
        $store->id = $request->id;
        $store->name = $request->name;
        $store->id_branch = $request->id_branch;
        $store->id_location = $request->id_location;
        $store->address = $request->address;
        $store->phone = $request->phone;
        $store->note = $request->note;

        $store->save();
        return redirect(route('stores.index'))->with('success', trans('main.Store Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = Store::find($id);
        return view('stores.edit',compact('store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:191',
            'address' => 'required|max:191',
            'id_branch' => 'required|not_in:0',
            'id_location' => 'required|not_in:0',
        ]);
        $store =  Store::find($id);
        $store->id = $request->id;
        $store->name = $request->name;
        $store->id_branch = $request->id_branch;
        $store->id_location = $request->id_location;
        $store->address = $request->address;
        $store->phone = $request->phone;
        $store->note = $request->note;

        $store->save();
        return redirect(route('stores.index'))->with('success', trans('main.Store Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = Store::find($id);
        $store->destroy($id);
        return redirect(route('stores.index'))->with('success', trans('main.Store Deleted Successfully'));
    }
}
