<?php

namespace App\Http\Controllers;

use App\Express;
use App\ExpressItem;
use App\ExpressStatus;
use App\Location;
use App\LocationPrice;
use Illuminate\Http\Request;
use Auth;
use DB;
class ExpressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
         return view('travels.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('travels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        // return $request;
        $expresses = Express::join('express_statuses','express_statuses.id_express','expresses.id')->select('expresses.id','expresses.from_location','expresses.to_location','expresses.from_branch','expresses.from_branch','expresses.created_at','expresses.total_delivery','expresses.package_price','expresses.id_store','expresses.tracking_no','expresses.dest_address','expresses.dest_phone',DB::raw('(select location from express_statuses where express_statuses.id_express=expresses.id and express_statuses.location='.Auth::user()->id.' limit 1) as locs'))->where('express_statuses.location', Auth::user()->id)->where('expresses.status',$request->status);

        if(isset($request->from_date))
        {
            $expresses=$expresses->where('expresses.created_at','>=',$request->from_date);
        }
        if(isset($request->to_date))
        {
            $expresses=$expresses->where('expresses.created_at','<=',$request->to_date);

        }
        $expresses=$expresses->distinct()->orderby('id','desc')->get();
        // return $expresses;
        $status = $request->status;
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        return view('travels.index',compact('expresses','status','from_date','to_date'));
    }
    public function printselected(Request $request)
    {
        // return $request;
        $ids = explode(',', $request->id_expressesp);
        $expresses = Express::whereIn('id',$ids)->get();
        return view('travels.print',compact('expresses'));

    }
    public function updatee(Request $request)
    {
        $express = Express::find($request->id);
        $express->dest_address = $request->val;
        $express->save();
        return "yes";
    }
    public function changestatus(Request $request)
    {
        // return $request;
        $ids = explode(',', $request->id_expresses);
        $cost = $request->cost/sizeof($ids);
        foreach ($ids as $i) {
            $st = new ExpressStatus();
            $st->id_express = $i;
            $st->location = $request->location;
            $st->cost = $cost;
            $st->note = $request->note;
            $st->driver_name = $request->driver_name;
            $st->driver_phone = $request->driver_phone;
            $st->driver_car_no = $request->driver_car_no;
            $st->id_user = Auth::user()->id;
            $st->status = $request->status;
            $st->save();
            $e = Express::find($st->id_express);
            $e->status = $request->status;
            // $e->id_location = $request->id_location;
            $e->save();
        }
        return redirect(route('travels.index'))->with('success', trans('main.Travel status changed Successfully'));
    }
    public function store(Request $request)
    {
        // return ($request);
        $request->validate([
            'id_store' => 'required',
            'user_id' => 'required',
            'from_location' => 'required|not_in:0',
            'to_location' => 'required|not_in:0',
            // 'from_branch' => 'required|not_in:0',
            // 'to_branch' => 'required|not_in:0',
            'date' => 'required|date',
            'p_type' => 'required|max:191',
            'd_price' => 'required|numeric',
            'p_price' => 'required|numeric',
            'total_price' => 'required|numeric',
            // 'dest_name' => 'required|max:191',
            'dest_phone' => 'required|max:191',
            // 'dest_address' => 'required|max:191',
        ]);
        $express =new Express();
        if(isset($request->dest_phone) && !isset($request->dest_address))
        $t = Express::where('dest_phone',$request->dest_phone)->orderby('id','desc')->first();

        if($t) $express->dest_address=$t->dest_address;
        else  $express->dest_address=$request->dest_address;
        $express->id_store=$request->id_store;
        $express->user_id=$request->user_id;
        $express->from_location=$request->from_location;
        $express->to_location=$request->to_location;
        $express->from_branch=$request->from_branch;
        $express->to_branch=$request->to_branch;
        $express->created_at=$request->date;
        $express->package_price=$request->p_price;
        $express->package_type=$request->p_type;
        $express->total_delivery=$request->d_price;
        $express->total_cost=$request->total_price;
        $express->dest_name=$request->dest_name;
        $express->dest_phone=$request->dest_phone;
        
        $express->status="Picked Up";
        $express->tracking_no=hexdec(uniqid());
        $express->save();
        $st = new ExpressStatus();
        $st->id_express = $express->id;
        $st->location = $express->from_location;
        $st->id_user = Auth::user()->id;
        $st->status = "Picked Up";
        $st->save();
        if(isset($request->item_name) && count($request->item_name)>0){
            foreach($request->item_name as $key=> $m){
                $data=array(
                    'id_express' => $express->id,
                    'item_name' => $request->item_name[$key],
                    'qty' => $request->item_qty[$key],
                    'price' => $request->item_price[$key],
                    'note' => $request->item_note[$key],
                );

                expressItem::insert($data);
            }
        }

        return redirect(route('travels.index'))->with('success', trans('main.Travel Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Express  $express
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $express = Express::find($id);
            return view('travels.show',compact('express'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Express  $express
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $express = Express::find($id);
        return view('travels.edit',compact('express'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Express  $express
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // return $request;
        $express = Express::find($id);
        $express->id_store=$request->id_store;
        $express->user_id=$request->user_id;
        $express->from_location=$request->from_location;
        $express->to_location=$request->to_location;
        $express->from_branch=$request->from_branch;
        $express->to_branch=$request->to_branch;
        $express->created_at=$request->date;
        $express->package_price=$request->p_price;
        $express->package_type=$request->p_type;
        $express->total_delivery=$request->d_price;
        $express->total_cost=$request->total_price;
        $express->dest_name=$request->dest_name;
        $express->dest_phone=$request->dest_phone;
        $express->dest_address=$request->dest_address;
        // $express->tracking_no=$request->tracking_no;
        $express->save();
        ExpressItem::where('id_express',$id)->delete();
        if(isset($request->item_name) && count($request->item_name)>0){
            foreach($request->item_name as $key=> $m){
                $data=array(
                    'id_express' => $express->id,
                    'item_name' => $request->item_name[$key],
                    'qty' => $request->item_qty[$key],
                    'price' => $request->item_price[$key],
                    'note' => $request->item_note[$key],
                );

                expressItem::insert($data);
            }
        }
                return redirect(route('travels.index'))->with('success', trans('main.Travel Updated Successfully'));
    }
    public function getPrice(Request $request)
    {
      // if($request->id>0){
        // return "not";

        $p = LocationPrice::where('from_location',$request->from_id)->where('to_location',$request->to_id)->orderby('created_at','desc')->first();
        // $date=Carbon::parse($request->date)->format('Y-m-d H:i:s');
        // $price = CostDate::where('created_at','<=',$date)->orderby('created_at','desc')->first();
        // $p = LocationCost::join('cost_dates','location_costs.id_date','cost_dates.id')->where('location_costs.from_id',$lo->from_id)->where('location_costs.to_id',$lo->to_id)->where('cost_dates.created_at',"<=",$date)->orderby('location_costs.id','desc')->first();
      // }else{
      //   // // return "yes";
      //   //   $p = LocationCost::where('from_id',$request->from_id)->where('to_id',$request->to_id)->orderby('id_date','desc')->first();
      // }
  return $p;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Express  $express
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $express = Express::find($id);
        ExpressItem::where('id',$id)->delete();
        ExpressStatus::where('id',$id)->delete();
        $express->destroy($id);
        return redirect(route('travels.index'))->with('success', trans('main.Travel Deleted Successfully'));
        // TripMaterial::where('trip_id',$id)->delete();
    }
}
