<?php

namespace App\Http\Controllers;

use App\ExpressStatus;
use App\Express;
use Illuminate\Http\Request;
use Auth;
class ExpressStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return $request;
        $status = new ExpressStatus();
        $status->id_express = $request->id_express;
        $status->id_user = Auth::user()->id;
        $status->status = $request->status;
        $status->location = $request->location;
        $status->cost = $request->cost;
        $status->note = $request->note;
        $status->driver_name = $request->driver_name;
        $status->driver_phone = $request->driver_phone;
        $status->driver_car_no = $request->driver_car_no;
        $status->save();
            $e = Express::find($status->id_express);
            $e->status = $request->status;
            $e->save();
        return redirect(route('travels.index'))->with('success', trans('main.Status Changed Successfully'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExpressStatus  $expressStatus
     * @return \Illuminate\Http\Response
     */
    public function show(ExpressStatus $expressStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExpressStatus  $expressStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(ExpressStatus $expressStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExpressStatus  $expressStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExpressStatus $expressStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExpressStatus  $expressStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExpressStatus $expressStatus)
    {
        //
    }
}
