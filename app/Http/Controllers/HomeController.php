<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('home');
        
    }
    public function sync($id)
    {
        // return "yes";
          $live_database = DB::connection('mysql_backup');
          $local_database = DB::connection('mysql');
     // Get table data from production
     foreach($local_database->table('expresses')->get() as $data){
        // return (array)$data;
        // Save data to staging database - default db connection
        $live_database->table('expresses')->insert((array) $data);
     }
     return '<span>داتاکانت بە سەرکەوتووی نێردرا بۆ سێرڤەر</span>';
     // Get table_2 data from production
     // foreach($live_database->table('table_2_name')->get() as $data){
     //    // Save data to staging database - default db connection
     //    DB::table('table_2_name')->insert((array) $data);
     // }
    }
}
