<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;
use Carbon\Carbon;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('locations.index');
    }

    public function create()
    {
        return view('locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:191',
        ]);

        $Location = new Location();
        $Location->name = $request->name;
        $Location->color = $request->name;

        $Location->save();
        return redirect(route('locations.index'))->with('success', trans('main.Location Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $Location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $Location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $Location
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Location = Location::find($id);
        return view('locations.edit',compact('Location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $Location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:191',
        ]);
        $Location =  Location::find($id);
        $Location->name = $request->name;
        $Location->color = $request->color;
        if($request->main ==1) $Location->main = 1;
        else $Location->main = 0;

        $Location->save();
        return redirect(route('locations.index'))->with('success', trans('main.Location Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $Location
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Location = Location::find($id);
        $Location->destroy($id);
        return redirect(route('locations.index'))->with('success', trans('main.Location Deleted Successfully'));
    }
}
