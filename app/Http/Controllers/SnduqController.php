<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class SnduqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $id_user = Auth::user()->id;
        return view('snduq.snduq',compact('id_user'));
    }
    public function snduq(Request $request)
    {
        //

}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $id_user = $request->id_user;
        return view('snduq.snduq',compact('id_user'));
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function today($id)
    {
        //
        $from_date = $id;
        $to_date = $id;
        return view('snduq.index',compact('from_date','to_date'));
    }

    public function todaypost(Request $request)
    {
        //
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        return view('snduq.index',compact('from_date','to_date'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
