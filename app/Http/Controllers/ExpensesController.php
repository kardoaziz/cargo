<?php

namespace App\Http\Controllers;

use App\ExpenseController;
use Illuminate\Http\Request;
use App\Expenses;
use Auth;
class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('expenses.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $f = new Expenses();
        $f->date = $request->date;
        $f->amount = $request->amount;
        $f->note = $request->note;
        $f->id_user = Auth::user()->id;
        $f->save();
        return redirect(route('expenses.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExpenseController  $expenseController
     * @return \Illuminate\Http\Response
     */
    public function show(ExpenseController $expenseController)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExpenseController  $expenseController
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $expense =  Expenses::find($id);
        return view('expenses.edit',compact('expense'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExpenseController  $expenseController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $f =  Expenses::find($id);
        $f->date = $request->date;
        $f->amount = $request->amount;
        $f->note = $request->note;
        $f->id_user = Auth::user()->id;
        $f->save();
        return redirect(route('expenses.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExpenseController  $expenseController
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $f =  Expenses::find($id);
        $f->destroy($id);
       
        return redirect(route('expenses.index'));
    }
}
