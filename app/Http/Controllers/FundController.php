<?php

namespace App\Http\Controllers;

use App\Fund;
use Illuminate\Http\Request;
use Auth;
class FundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('funds.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $f = new Fund();
        $f->date = $request->date;
        $f->amount = $request->amount;
        $f->note = $request->note;
        $f->id_user = Auth::user()->id;
        $f->save();
        return redirect(route('funds.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fund  $fund
     * @return \Illuminate\Http\Response
     */
    public function show(Fund $fund)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fund  $fund
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $fund =  Fund::find($id);
        return view('funds.edit',compact('fund'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fund  $fund
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $f =  Fund::find($id);
        $f->date = $request->date;
        $f->amount = $request->amount;
        $f->note = $request->note;
        $f->id_user = Auth::user()->id;
        $f->save();
        return redirect(route('funds.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fund  $fund
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $f =  Fund::find($id);
        $f->destroy($id);
       
        return redirect(route('funds.index'));
    }
}
