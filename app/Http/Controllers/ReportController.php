<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Express;
use App\ExpressStatus;
class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('reports.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request)
    {
        $expresses = Express::where('created_at','>=',$request->date.' 00:00:00')->where('created_at','<=',$request->date.' 23:59:59')->get();
         foreach ($expresses as $e) {
                 $e->total_expense = ExpressStatus::where('id_express',$e->id)->sum('cost'); 
             }
        $date = $request->date;

        return view('reports.detail',compact('expresses','date'));
    }
    public function store(Request $request)
    {
        //
        // return $request;
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $from_location = $request->from_location;
        $to_location = $request->to_location;
        $store = $request->id_store;
        $status = $request->status;
        $type='';
       
        if(isset($request->total)){
            $type="total";
             if($request->status!="0" && $request->status!="undelivered"){
                $expresses = Express::join('express_statuses','express_statuses.id_express','expresses.id')->where('express_statuses.status',$request->status)->where('expresses.created_at','>=',$from_date.' 00:00:00')->where('expresses.created_at','<=',$to_date.' 23:59:59')->select('expresses.created_at',DB::raw(' sum(expresses.package_price) as total_price'),DB::raw(' sum(expresses.total_delivery) as total_delivery'),DB::raw('(select sum(express_statuses.cost) from express_statuses where express_statuses.id_express=expresses.id) as total_expenses'),DB::raw('count(*) as no_package'))->distinct()->groupby('expresses.created_at');
             }
             else
             {
             $expresses = Express::where('created_at','>=',$from_date.' 00:00:00')->where('created_at','<=',$to_date.' 23:59:59')->select('created_at',DB::raw(' sum(package_price) as total_price'),DB::raw(' sum(total_delivery) as total_delivery'),DB::raw('(select sum(express_statuses.cost) from express_statuses where express_statuses.id_express=expresses.id) as total_expenses'),DB::raw('count(*) as no_package'))->groupby('created_at');
            }
             if($request->id_store>0)
             {
                $expresses = $expresses->where('id_store',$request->id_store);
             }
             if($request->from_location>0)
             {
                $expresses = $expresses->where('from_location',$request->from_location);
             }
             if($request->to_location>0)
             {
                $expresses = $expresses->where('to_location',$request->to_location);
             }
             if($request->status!="0")
             {
                if($request->status=="undelivered"){
                    $expresses = $expresses->where('expresses.status','<>','Delivered');
                }
                // else{
                //     $expresses = $expresses->where('expresses.status',$request->status);
                // }
                
             }
             $expresses=$expresses->get();
        }
        else{
            $type="detail";
             if($request->status!="0" && $request->status!="undelivered"){
                 $expresses = Express::join('express_statuses','express_statuses.id_express','expresses.id')->where('express_statuses.status',$request->status)->where('expresses.created_at','>=',$from_date.' 00:00:00')->where('expresses.created_at','<=',$to_date.' 23:59:59');
             }else{

            $expresses = Express::where('created_at','>=',$from_date.' 00:00:00')->where('created_at','<=',$to_date.' 23:59:59');
             }
            if($request->id_store>0)
             {
                $expresses = $expresses->where('expresses.id_store',$request->id_store);
             }
             if($request->from_location>0)
             {
                $expresses = $expresses->where('expresses.from_location',$request->from_location);
             }
             if($request->to_location>0)
             {
                $expresses = $expresses->where('expresses.to_location',$request->to_location);
             }
             if($request->status!="0")
             {
                if($request->status=="undelivered"){
                    $expresses = $expresses->where('expresses.status','<>','Delivered')->where('expresses.status','<>','ReturnedSeller')->where('expresses.status','<>','Returned');
                }
                // else{
                //     $expresses = $expresses->where('expresses.status',$request->status);
                // }
             }
            $expresses = $expresses->get();
            foreach ($expresses as $e) {
                 $e->total_expense = ExpressStatus::where('id_express',$e->id)->sum('cost'); 
             }
        }
       
        // 
        return view('reports.index',compact('expresses','type','from_date','to_date','from_location','to_location','store','status'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
