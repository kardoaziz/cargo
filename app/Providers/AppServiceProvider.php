<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use \App\Transfer;
use \App\ExpressStatus;
use \App\Express;
use Auth;
use DB;
use Carbon\Carbon;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        //
       //   Schema::defaultStringLength(191);

   $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
    $event->menu->add(
        [
            'text' => 'پاکێجەکان',
            'url'  => 'travels',
            'icon' =>'fas fa-mail-bulk',
            'can' =>'view_package',
            'label'       =>  ExpressStatus::where('location', Auth::user()->id_location)->whereDate('created_at', DB::raw('CURDATE()'))->whereIn('status',['Delivered','Redirected'])->select('id_express')->distinct()->count(),
        'label_color' => 'warning'
        ], [
            'text' => 'پاکێجەکانم',
            'url'  => 'mytravels',
            'icon' =>'fas fa-mail-bulk',
            'can' => 'mypackages',
            'label'       =>  Express::where('id_store', Auth::user()->id_store)->whereDate('created_at', DB::raw('CURDATE()'))->select('id_express')->distinct()->count(),
        'label_color' => 'warning'
        ]
        , [
            'text' => 'سندوق',
            'url'  => route('snduq.create'),
            'icon' =>'fa fa-university',
            'can' => 'show_snduq',
            
        ],[
        'text'        => 'حەواڵە',
        'url'         => 'transfer',
        'icon'        => 'fas fa-fw fa-exchange-alt',
        'can' =>'view_transfer',
        'label'       => Transfer::where('to_user',Auth::user()->id)->where('status','pending')->count(),
        'label_color' => 'danger',
    ],[
        'text'        => 'خەرجی',
        'url'         => 'expenses',
        'icon'        => 'fas fa-fw fa-file-invoice-dollar',
        'can' =>'view_expense',
        
    ],
    [
        'text'        => 'ڕاپۆرت',
        'url'         => 'reports',
        'icon'        => 'fa fa-fw fa-clipboard',
        // 'can' =>'view_transfer',
      
    ],[
        'text'        => 'دەربارەی ئێمە',
        'url'         => 'aboutus',
        'icon'        => 'fas fa-fw fa-info',
        
    ],[
        'text'        => '',
        'url'         => 'transfer',
        'icon'        => 'fas fa-fw fa-exchange-alt',
        'can' =>'view_transfer',
        'topnav' =>true,
        'label'       => Transfer::where('to_user',Auth::user()->id)->where('status','pending')->count(),
        'label_color' => 'danger',
    ],[
        'text'        => '',
        'url'         => 'travels',
        'icon'        => 'fas fa-mail-bulk',
        'can' =>'view_package',
        'topnav' =>true,
        'label'       =>  ExpressStatus::where('location', Auth::user()->id_location)->whereDate('created_at', DB::raw('CURDATE()'))->whereIn('status',['Delivered','Redirected'])->select('id_express')->distinct()->count(),
        'label_color' => 'warning',
    ],[
        'text'        => '',
        'url'         => '/',
        'icon'        => 'fas fa-home',
        'can' =>'view_home',
        'topnav' =>true,
        
    ]);
});
    }
}
