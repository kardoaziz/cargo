<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use \App\Transfer;
use Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        //
       //   Schema::defaultStringLength(191);

   $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
    $event->menu->add([
        'text'        => 'حەواڵە',
        'url'         => 'transfer',
        'icon'        => 'fas fa-fw fa-exchange-alt',
        'label'       => Transfer::where('to_user',Auth::user()->id)->where('status','pending')->count(),
        'label_color' => 'danger',

    ]);
});
    }
}
