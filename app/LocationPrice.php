<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationPrice extends Model
{
    //
    public function fromlocation()
    {
        return $this->belongsTo('App\Location','from_location');
    }
    public function tolocation()
    {
        return $this->belongsTo('App\Location','to_location');
    }
}
