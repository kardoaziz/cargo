<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
     public function froms()
    {
        return $this->belongsTo('App\Express','from_location');
    }
     public function tos()
    {
        return $this->belongsTo('App\Express','to_location');
    }
    public function status()
    {
        return $this->hasMany('App\ExpressStatus','location');
    }
     public function users()
    {
        return $this->hasMany('App\User','id_location');
    }
    public function fromprices()
    {
        return $this->hasMany('App\LocationPrice','from_location');
    }
     public function toprices()
    {
        return $this->hasMany('App\LocationPrice','to_location');
    }
}
