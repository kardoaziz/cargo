<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpressStatus extends Model
{
    //
     public function express()
    {
        return $this->belongsTo('App\Express','id_express');
    }
     public function user()
    {
        return $this->belongsTo('App\User','id_user');
    }
    public function Location()
    {
        return $this->belongsTo('App\Location','location');
    }
}
