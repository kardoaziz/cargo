<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    //
     public function expresses()
    {
        return $this->hasMany('App\Express','id_store');
    }

    public function location()
    {
        return $this->belongsTo('App\Location','id_location');
    }
    public function branch()
    {
        return $this->belongsTo('App\Branch','id_branch');
    }
     public function user()
    {
        return $this->hasOne('App\User','id_store');
    }
}
