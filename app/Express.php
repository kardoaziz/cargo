<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Express extends Model
{
    //
    public function store()
    {
        return $this->belongsTo('App\Store','id_store');
    }
    public function from_branch()
    {
        return $this->belongsTo('App\Branch','from_branch');
    }
    public function to_branch()
    {
        return $this->belongsTo('App\Branch','to_branch');
    }
    public function fromlocation()
    {
        return $this->belongsTo('App\Location','from_location');
    }
    public function tolocation()
    {
        return $this->belongsTo('App\Location','to_location');
    }
    public function items()
    {
        return $this->hasMany('App\ExpressItem','id_express');
    }
    public function statuses()
    {
        return $this->hasMany('App\ExpressStatus','id_express');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    //  public function tos()
    // {
    //     return $this->belongsTo('App\Express','to_location');
    // }
}
