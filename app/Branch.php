<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    //
    public function froms()
    {
        return $this->belongsTo('App\Express','from_branch');
    }
     public function tos()
    {
        return $this->belongsTo('App\Express','to_branch');
    }
    public function users()
    {
        return $this->hasMany('App\User','id_branch');
    }
        public function usr()
    {
        return $this->belongsTo('App\User','id_user');
    }
}
