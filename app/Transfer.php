<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    //
     public function from_users()
    {
        return $this->belongsTo('App\User','from_user');
    }
    public function to_users()
    {
        return $this->belongsTo('App\User','to_user');
    }
}
