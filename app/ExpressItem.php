<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpressItem extends Model
{
    //
    public function express()
    {
        return $this->belongsTo('App\Express','id_express');
    }
}
