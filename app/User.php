<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','id_branch','id_location','id_store','phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
     public function from_transfers()
    {
        return $this->hasMany('App\Transfer','from_user');
    }
    public function to_transfers()
    {
        return $this->hasMany('App\Transfer','to_user');
    }
    public function statuses()
    {
        return $this->hasMany('App\ExpressStatus','id_user');
    }
    public function store()
    {
        return $this->belongsTo('App\Store','id_store');
    }
    public function express()
    {
        return $this->hasMany('App\Express','user_id');
    }
    public function branch()
    {
        return $this->belongsTo('App\Branch','id_branch');
    }
    public function location()
    {
        return $this->belongsTo('App\Location','id_location');
    }
}
