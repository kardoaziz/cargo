<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expresses', function (Blueprint $table) {
            $table->id();
            $table->integer('id_store')->unsigned();
            $table->integer('from_branch')->unsigned();
            $table->integer('to_branch')->unsigned();
            $table->integer('from_location')->unsigned();
            $table->integer('to_location')->unsigned();
            $table->string('tracking_no', 191);
            $table->string('dest_name', 191);
            $table->string('dest_phone', 191);
            $table->string('dest_address', 191);
            $table->integer('total_delivery')->unsigned();
            $table->integer('total_cost')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expresses');
    }
}
