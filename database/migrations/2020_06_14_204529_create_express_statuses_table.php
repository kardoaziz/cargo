<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpressStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('express_statuses', function (Blueprint $table) {
            $table->id();
            $table->integer('id_express')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->integer('cost')->default(0);
            $table->string('status', 191);
            $table->string('driver_name', 191);
            $table->string('driver_phone', 191);
            $table->string('driver_car_no', 191);
            $table->string('note', 191)->nullable();
            $table->string('name_wargr', 191);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('express_statuses');
    }
}
