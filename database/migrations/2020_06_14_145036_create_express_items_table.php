<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpressItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('express_items', function (Blueprint $table) {
            $table->id();
            $table->integer('id_express')->unsigned();
            $table->string('item_name', 191)->nullable();
            $table->integer('qty')->default(0);
            $table->integer('price')->default(0)->nullable();
            $table->string('note', 191);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('express_items');
    }
}
