<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_us', function (Blueprint $table) {
            $table->id();
            $table->LongText('description')->nullable();
            $table->LongText('facebook')->nullable();
            $table->LongText('instagram')->nullable();
            $table->LongText('snapchat')->nullable();
            $table->LongText('twitter')->nullable();
            $table->LongText('youtube')->nullable();
            $table->LongText('website')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_us');
    }
}
